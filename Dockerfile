# base image

FROM node:latest

WORKDIR /app

ADD . /app

# image creation time dependencies
RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
